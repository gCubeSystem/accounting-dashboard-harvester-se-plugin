package org.gcube.dataharvest.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.ws.rs.InternalServerErrorException;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.JWTSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.keycloak.KeycloakClientFactory;
import org.gcube.common.keycloak.model.TokenResponse;
import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.dataharvest.AccountingDashboardHarvesterPlugin;
import org.gcube.resourcemanagement.support.server.managers.context.ContextManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextAuthorization {
	
	private static Logger logger = LoggerFactory.getLogger(ContextAuthorization.class);
	
	public static final String CLIENT_ID = "accounting-dashboard-harvester-se-plugin";
	
	protected String clientSecret;
	
	/**
	 * Contains Context full name as key and Token as Value
	 */
	protected Map<String,Secret> contextToToken;
	
	/**
	 * Contains Token as key and Context full name as Value
	 */
	protected Map<Secret,String> tokenToContext;
	
	protected Properties properties;
	
	/**
	 * Contains  Properties used to generate tokens
	 */
	public ContextAuthorization(Properties properties) throws Exception {
		this.properties = properties;
		this.contextToToken = new HashMap<>();
		this.tokenToContext = new HashMap<>();
		retrieveContextsAndTokens();
	}
	
	/**
	 * Contains  Properties used to generate tokens
	 */
	public ContextAuthorization() throws Exception {
		this.properties = AccountingDashboardHarvesterPlugin.getConfigParameters();
		this.contextToToken = new HashMap<>();
		this.tokenToContext = new HashMap<>();
		retrieveContextsAndTokens();
	}
	
	private String getClientSecret(String context) {
		try {
			if(clientSecret==null) {
				int index = context.indexOf('/', 1);
				String root = context.substring(0, index == -1 ? context.length() : index);
				clientSecret = properties.getProperty(root);
			}
			return clientSecret;
		} catch(Exception e) {
			throw new InternalServerErrorException(
					"Unable to retrieve Application Token for context " + SecretManagerProvider.instance.get().getContext(), e);
		}
	}
	
	private TokenResponse getJWTAccessToken(String context) throws Exception {
		TokenResponse tr = KeycloakClientFactory.newInstance().queryUMAToken(context, CLIENT_ID, getClientSecret(context), context, null);
	    return tr;
	}
	
	public Secret getCatalogueSecretForContext(String context) throws Exception {
		TokenResponse tr = getJWTAccessToken(context);
		Secret secret = new JWTSecret(tr.getAccessToken());
		return secret;
	}
	
	protected void retrieveContextsAndTokens() throws Exception {
		try {
			
			LinkedHashMap<String,ScopeBean> map = ContextManager.readContexts();
			
			for(String scope : map.keySet()) {
				try {
					String context = map.get(scope).toString();
					
					Secret secret = getCatalogueSecretForContext(context);
							
					contextToToken.put(context, secret);
					tokenToContext.put(secret, context);
					
				} catch(Exception e) {
					logger.error("Error while elaborating {}", scope, e);
					// throw e;
				} 
				
			}
		} catch(Exception ex) {
			throw ex;
		} 
	}
	
	public Secret getSecretForContext(String context) {
		return contextToToken.get(context);
	}
	
	public String getContextFromSecret(Secret secret) {
		return tokenToContext.get(secret);
	}
	
	public SortedSet<String> getContexts() {
		return new TreeSet<String>(contextToToken.keySet());
	}
	
}
