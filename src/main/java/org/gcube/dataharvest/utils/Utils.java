package org.gcube.dataharvest.utils;

import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Eric Perrone (ISTI - CNR)
 * @author Luca Frosini (ISTI - CNR)
 */
public class Utils {
	
	private static Logger logger = LoggerFactory.getLogger(Utils.class);
	
	public static String getCurrentContext() throws ObjectNotFound, Exception {
		return SecretManagerProvider.instance.get().getContext();
	}
	
	public static void setContext(Secret secret) throws Exception {
		SecretManagerProvider.instance.reset();
		SecretManager secretManager = new SecretManager(); 
		SecretManagerProvider.instance.set(secretManager);
		secretManager.addSecret(secret);
		secretManager.set();
	}
	
}
