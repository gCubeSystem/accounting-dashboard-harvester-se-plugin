package org.gcube.dataharvest.harvester;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.gcube.accounting.accounting.summary.access.model.ScopeDescriptor;
import org.gcube.accounting.accounting.summary.access.model.update.AccountingRecord;
import org.gcube.dataharvest.AccountingDashboardHarvesterPlugin;
import org.gcube.dataharvest.datamodel.HarvestedDataKey;
import org.gcube.social_networking.social_networking_client_library.UserClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Eric Perrone (ISTI - CNR)
 * @author Luca Frosini (ISTI - CNR)
 */
public class VREUsersHarvester extends BasicHarvester {

	private static Logger logger = LoggerFactory.getLogger(VREUsersHarvester.class);


	public static final String PATH = "/2/users/get-all-usernames?gcube-token=";

	public VREUsersHarvester(Date start, Date end) throws Exception {
		super(start, end);
	}

	@Override
	public List<AccountingRecord> getAccountingRecords() throws Exception {
		try {
			String context = org.gcube.dataharvest.utils.Utils.getCurrentContext();
			
			int measure = get();
			
			ArrayList<AccountingRecord> accountingRecords = new ArrayList<AccountingRecord>();
			
			ScopeDescriptor scopeDescriptor = AccountingDashboardHarvesterPlugin.getScopeDescriptor(context);
			
			AccountingRecord ar = new AccountingRecord(scopeDescriptor, instant, getDimension(HarvestedDataKey.USERS), (long) measure);
			logger.debug("{} : {}", ar.getDimension().getId(), ar.getMeasure());
			accountingRecords.add(ar);
			
			return accountingRecords;
			
		} catch(Exception e) {
			throw e;
		}
	}

	private int get() throws Exception {
		UserClient userClient = new UserClient();
		return userClient.getAllUsernamesContext().size();
	}

}
