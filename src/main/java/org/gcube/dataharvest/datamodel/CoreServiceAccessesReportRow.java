package org.gcube.dataharvest.datamodel;

public class CoreServiceAccessesReportRow {
	private String dashboardContext;
	private HarvestedDataKey key;
	private String pagePath;
	private int visitNumber;
	
	public CoreServiceAccessesReportRow() {
		// TODO Auto-generated constructor stub
	}
	public HarvestedDataKey getKey() {
		return key;
	}
	public void setKey(HarvestedDataKey key) {
		this.key = key;
	}
	public String getPagePath() {
		return pagePath;
	}
	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}
	public int getVisitNumber() {
		return visitNumber;
	}
	public void setVisitNumber(int visitNumber) {
		this.visitNumber = visitNumber;
	}
	public String getDashboardContext() {
		return dashboardContext;
	}
	public void setDashboardContext(String dashboardContext) {
		this.dashboardContext = dashboardContext;
	}
	@Override
	public String toString() {
		return "CoreServiceAccessesReportRow [dashboardContext=" + dashboardContext + ", key=" + key + ", pagePath="
				+ pagePath + ", visitNumber=" + visitNumber + "]";
	}
	
	

}
