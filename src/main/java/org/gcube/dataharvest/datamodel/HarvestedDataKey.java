/**
 *
 */
package org.gcube.dataharvest.datamodel;


/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @author M. Assante, ISTI-CNR
 */
public enum HarvestedDataKey {

	WORKSPACE_ACCESSES("Workspace Accesses"),
	MESSAGES_ACCESSES("Messages Accesses"),
	NOTIFICATIONS_ACCESSES("Notifications Accesses"),
	PROFILE_ACCESSES("Profile Accesses"),
	JUPYTER_ACCESSES("Jupyter Accesses"),
	RSTUDIO_ACCESSES("R Studio Accesses"),
	
	CATALOGUE_ACCESSES("Catalogue Accesses"),
	CATALOGUE_DATASET_LIST_ACCESSES("Item List"),
	CATALOGUE_DATASET_ACCESSES("Item Metadata"),
	CATALOGUE_RESOURCE_ACCESSES("Item Resource"),
	ACCESSES("VRE Accesses"),
	USERS("VRE Users"),
	
	SOCIAL_POSTS("VRE Social Interations Posts"),
	SOCIAL_REPLIES("VRE Social Interations Replies"),
	SOCIAL_LIKES("VRE Social Interations Likes"),
	METHOD_INVOCATIONS("VRE Methods Invocation");

	private String key;

	HarvestedDataKey(String key){
		this.key = key;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

}
