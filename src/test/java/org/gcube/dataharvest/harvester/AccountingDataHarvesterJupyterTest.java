package org.gcube.dataharvest.harvester;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;

import org.gcube.accounting.accounting.summary.access.AccountingDao;
import org.gcube.accounting.accounting.summary.access.model.update.AccountingRecord;
import org.gcube.dataharvest.ContextTest;
import org.gcube.dataharvest.plugin.AccountingDataHarvesterPluginTest;
import org.gcube.dataharvest.utils.AggregationType;
import org.gcube.dataharvest.utils.ContextAuthorization;
import org.gcube.dataharvest.utils.DateUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi (ISTI CNR)
 *
 */
public class AccountingDataHarvesterJupyterTest extends AccountingDataHarvesterPluginTest {

	private static Logger logger = LoggerFactory.getLogger(AccountingDataHarvesterJupyterTest.class);
	
	@Ignore
	@Test
	public void testJupyterAccessesHarvester() throws Exception {
		try {

			ContextTest.setContextByName(ROOT_PROD);
			AccountingDao dao = getAccountingDao();

			List<Date> starts = new ArrayList<>();
			starts.add(DateUtils.getStartCalendar(2021, Calendar.JANUARY, 1).getTime());
			starts.add(DateUtils.getStartCalendar(2021, Calendar.FEBRUARY, 1).getTime());
			starts.add(DateUtils.getStartCalendar(2021, Calendar.MARCH, 1).getTime());
			starts.add(DateUtils.getStartCalendar(2021, Calendar.APRIL, 1).getTime());
			starts.add(DateUtils.getStartCalendar(2021, Calendar.MAY, 1).getTime());
			
			AggregationType measureType = AggregationType.MONTHLY;

			ContextAuthorization contextAuthorization = new ContextAuthorization();
			SortedSet<String> contexts = contextAuthorization.getContexts();
			/*
			SortedSet<String> contexts = new TreeSet<>();
			contexts.add("/d4science.research-infrastructures.eu/D4OS/Blue-CloudLab");
			contexts.add("/d4science.research-infrastructures.eu/D4OS/Zoo-Phytoplankton_EOV");
			contexts.add("/d4science.research-infrastructures.eu/D4OS/MarineEnvironmentalIndicators");
			*/
			
			List<AccountingRecord> accountingRecords = new ArrayList<>();
			
			for (Date start : starts) {
				Date end = DateUtils.getEndDateFromStartDate(measureType, start, 1);

				ContextTest.setContextByName(ROOT_PROD);
				JupyterAccessesHarvester jupyterAccessesHarvester = new JupyterAccessesHarvester(start, end);

				for(String context : contexts) {
					ContextTest.setContextByName(context);
					
					List<AccountingRecord> harvested = jupyterAccessesHarvester.getAccountingRecords();
					accountingRecords.addAll(harvested);
				}

			}

			// logger.debug("{}", accountingRecords);

			logger.debug("Going to insert {}", accountingRecords);
			
			ContextTest.setContextByName(ROOT_PROD);
			
			// dao.insertRecords(accountingRecords.toArray(new AccountingRecord[1]));

		} catch (Throwable e) {
			logger.error(e.getLocalizedMessage(), e);
			throw e;
		}
	}

	
}
