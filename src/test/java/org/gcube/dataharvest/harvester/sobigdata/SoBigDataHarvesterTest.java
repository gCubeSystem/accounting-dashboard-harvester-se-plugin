package org.gcube.dataharvest.harvester.sobigdata;

import java.util.List;

import org.gcube.dataharvest.ContextTest;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoBigDataHarvesterTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(SoBigDataHarvesterTest.class);
	
	@Ignore
	// @Test
	public void testGroupList() throws Exception {
		// ContextTest.setContextByName("/d4science.research-infrastructures.eu/D4Research/AGINFRAplusDev");
		// ContextTest.setContextByName("/d4science.research-infrastructures.eu/gCubeApps/SIASPA");
		List<String> groups = SoBigDataHarvester.listGroup();
		logger.debug("{}", groups);
	}
	
}
