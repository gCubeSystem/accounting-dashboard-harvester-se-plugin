package org.gcube.dataharvest.harvester;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.stream.Stream;

import org.gcube.accounting.accounting.summary.access.AccountingDao;
import org.gcube.accounting.accounting.summary.access.model.update.AccountingRecord;
import org.gcube.dataharvest.ContextTest;
import org.gcube.dataharvest.plugin.AccountingDataHarvesterPluginTest;
import org.gcube.dataharvest.utils.AggregationType;
import org.gcube.dataharvest.utils.ContextAuthorization;
import org.gcube.dataharvest.utils.DateUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi (ISTI CNR)
 *
 */
public class AccountingDataHarvesterRStudioTest extends AccountingDataHarvesterPluginTest {

	private static Logger logger = LoggerFactory.getLogger(AccountingDataHarvesterRStudioTest.class);

	
	@Ignore
	@Test
	public void testJupyterAccessesHarvester() throws Exception {
		try {

			ContextTest.setContextByName(ROOT_PROD);
			AccountingDao dao = getAccountingDao();

			List<Date> starts = new ArrayList<>();
			
			LocalDate sdate = LocalDate.parse("2016-01-01"), edate = LocalDate.parse("2021-06-01");

			Stream.iterate(sdate, date -> date.plusMonths(1)).limit(ChronoUnit.MONTHS.between(sdate, edate) + 1)
					.forEach(dateToConvert -> starts.add(java.util.Date
							.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())));
			
			AggregationType measureType = AggregationType.MONTHLY;
			
			ContextAuthorization contextAuthorization = new ContextAuthorization();
			SortedSet<String> contexts = contextAuthorization.getContexts();
			
			List<AccountingRecord> accountingRecords = new ArrayList<>();
			
			
			for (Date start : starts) {
				Date end = DateUtils.getEndDateFromStartDate(measureType, start, 1);

				ContextTest.setContextByName(ROOT_PROD);
				RStudioAccessesHarvester rstudioAccessesHarvester = new RStudioAccessesHarvester(start, end);

				for(String context : contexts) {
					ContextTest.setContextByName(context);
					
					List<AccountingRecord> harvested = rstudioAccessesHarvester.getAccountingRecords();
					accountingRecords.addAll(harvested);
				}

			}

			// logger.debug("{}", accountingRecords);

			logger.debug("Going to insert {}", accountingRecords);
			
			ContextTest.setContextByName(ROOT_PROD);
			dao.insertRecords(accountingRecords.toArray(new AccountingRecord[1]));

		} catch (Throwable e) {
			logger.error(e.getLocalizedMessage(), e);
			throw e;
		}
	}

	
}
