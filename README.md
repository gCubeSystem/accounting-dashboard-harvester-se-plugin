# Accounting Dashboard Harvester Smart Executor Plugin

Accounting Dashboard Harvester Smart Executor Plugin harvest accounting data 
from different sources, harmonize them and store in a PostgreSQL database to
enable aggregated infrastructure analysis.

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

[Accounting Dashboard Harvester Smart Executor Plugin](https://wiki.gcube-system.org/gcube/Accounting)

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/accounting-lib/releases).

## Authors

* **Luca Frosini** ([ORCID](https://orcid.org/0000-0003-3183-2291)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)
* **Massimiliano Assante** ([ORCID](https://orcid.org/0000-0002-3761-1492)) - - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)
* **Francesco Mangiacrapa** ([ORCID](https://orcid.org/0000-0002-6528-664X)) - - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## How to Cite this Software

Tell people how to cite this software. 
* Cite an associated paper?
* Use a specific BibTeX entry for the software?


    @Manual{,
        title = {Accounting Dashboard Harvester Smart Executor Plugin},
        author = {{Frosini, Luca}, {Assante, Massimiliano}, {Mangiacrapa, Francesco}},
        organization = {ISTI - CNR},
        address = {Pisa, Italy},
        year = 2019,
        url = {http://www.gcube-system.org/}
    } 

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)
