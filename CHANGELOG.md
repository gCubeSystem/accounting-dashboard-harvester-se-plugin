This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "accounting-dashboard-harvester-se-plugin"

## [v2.5.0]

- Upgraded smart-executor-bom to 3.3.0 [#27999]


## [v2.4.0]

- Removed filter restriction on JobUsageRecord harvesting to get MethodInvocation
- Fixed bug on getting ScopeDescriptor for new scopes.


## [v2.3.0]

- Ported GA harvesters to Analytics Data API (GA4)


## [v2.2.0]

- Switching security to the new IAM [#21904]


## [v2.1.0]

- Storagehub-client-library get range from gcube-bom [#22822]


## [v2.0.0]

- Ported plugin to smart-executor APIs 3.0.0 [#21616]
- Added RStudio Harvester [#21557]
- Added Jupyter Harvester [#21031]
- Switched accounting JSON management to gcube-jackson [#19115]
- Switched smart-executor JSON management to gcube-jackson [#19647]


## [v1.6.0] - 2020-05-22

- [#19047] Added core services accesses 


## [v1.5.0] - 2020-03-30

- [#18290] Google Analytics Plugin for Catalogue pageviews 
- [#18848] Updated Catalogue Dashboard harvester ENUM 


## [v1.4.0] - 2019-12-19

- [#17800] Allowed partial harvesting of the current period 


## [v1.3.0] - 2019-11-06

- [#17800] Allowed partial harvesting of the current period 


## [v1.2.0] - 2019-09-11

- [#17128] Removed Home Library dependency 
- [#17128] Removed ckan-util-library dependency


## [v1.1.0] [r4.13.1] - 2019-02-26

- [#12985] Fixed scope of dependencies


## [v1.0.0] [r4.13.1] - 2018-10-10

- First Release

